from datetime import datetime, timedelta, time
import numpy as np
import math, julian

Mu = 0.3998603e6
JD2000 = 2451545.0
We = 7.2921151467e-5



class Orbit:
    __epoch__ = None
    time = 0.0
    Rp = 0.0
    e = 0.0
    ArgLat = 0.0
    i = 0.0
    AscNode = 0
    ArgPerigee = 0.0
    Pos = np.zeros(3)
    Velocity = np.zeros(3)
    Geo = {'pos': np.zeros(3), 'R': 0.0, 'lat': 0.0, 'lon': 0.0}


    def __init__(self, epoch):
        self.__epoch__ = epoch

    def assign(self, rp, arglat, i, argperigee, ascnode, e):
        self.Rp = rp
        self.Arglat = arglat
        self.i = i
        self.ArgPerigee = argperigee
        self.AscNode = ascnode
        self.e = e
        self.to_decart()
        self.to_geo()

    def mean_anomaly(self):
        E = self.eccentricity_anomaly()
        r = E - self.e * math.sin(self.e)
        if r < 0:
            r = math.pi * 2 + r
        return r

    '@property'
    def true_anomaly(self):
        '''
        Здесь какой-то метод.
        return
        '''
        r = self.ArgLat - self.ArgPerigee
        if r < 0: r += 2*math.pi
        return r

    def parameter(self):
        return self.Rp*(1+self.e)

    def radius(self):
        return self.parameter() / (1 + self.e * math.cos(self.true_anomaly()))

    def period(self):
        return math.twopi * self.semi_major_axis() * math.sqrt(self.semi_major_axis() / Mu)

    def mean_motion(self):
        return math.twopi / self.period()

    def radial_velocity(self):
        return math.sqrt(Mu / self.parameter()) * self.e * math.sin(self.true_anomaly())

    def semi_major_axis(self):
        return self.Rp / (1 - self.e)

    def eccentricity_anomaly(self):
        v = self.true_anomaly()
        r = 2 * math.atan(math.sqrt((1 - self.e) / (1 + self.e)) * math.tan(0.5 * v));
        if r < 0.0: r = math.pi * 2 + r
        return r

    def perigee_time(self):
        return - self.mean_anomaly() / self.mean_motion()

    def to_decart(self):
        Su = math.sin(self.ArgLat)
        So = math.sin(self.AscNode)
        Si = math.sin(self.i)
        Cu = math.cos(self.ArgLat)
        Co = math.cos(self.AscNode)
        Ci = math.cos(self.i)
        r = self.radius()
        self.Pos[0] = (Cu * Co - Su * So * Ci) * r
        self.Pos[1] = (Cu * So + Su * Co * Ci) * r
        self.Pos[2] = Si * Su * r
        Vr_r = self.radial_velocity() / r
        Rw = self.AngularRate() * r
        self.velocity[0] = Vr_r * self.Pos[0] - Rw * (Su * Co - Cu * So * Ci)
        self.velocity[1] = Vr_r * self.Pos[1] - Rw * (Su * Co + Cu * Co * Ci)
        self.velocity[2] = Vr_r * self.Pos[2] - Rw * Si * Cu

    def to_geo(self):
        s = siderial_time(self.__epoch__)
        m = mabs_geo(s)
        self.Geo['pos'] = np.dot(m, self.Pos)
        self.Geo['R'] = np.Linalg.norm(self.Geo['pos'])
        self.Geo['lat'] = math.asin(self.Geo['pos'][2] / self.Geo['R'])
        w = math.sqrt(math.pow(self.Geo['pos'][0], 2) + math.pow(self.Geo['pos'][1], 2))
        lc = self.Geo['pos'][0] / w
        ls = self.Geo['pos'][1] / w
        if ls > 0:
            self.Geo['lon'] = math.acos(lc)
        else:
            self.Geo['lon'] = - math.acos(lc)


class Spacecraft:
    time = 0.0

    def __init__(self, epoch):
        self.inital_orbit = Orbit(epoch)
        self.current_orbit = Orbit(epoch)

    def set_current_time(self, val):
        n = self.initial_orbit.mean_motion()
        tp = self.initial_orbit.perigee_time()
        m = n * (val - tp)
        ecc = self.initial_orbit.e
        e = self.kepler_equation(ecc, m)
        v = 2 * math.atan(math.sqrt((1 + ecc) / (1 - ecc)) * math.tan(0.5 * e))
        if v < 0:
            v = math.twopi + v
        u = v + self.current_orbit.ArgPerigee
        if u > math.twopi:
            u = u - math.twopi
        self.current_orbit.__epoch__ = self.initial_orbit.__epoch__ - timedelta(seconds=val) / 86400

        self.current_orbit.assign(self.initial_orbit.Rp.u, self.initial_orbit.i, self.inital_orbit.ArgPerigee, self.initial_orbit.AscNode, self.initial_orbit.e,)

    def kepler_equation(self, e, M):
        old = M
        Result = M + e * math.sin(old)
        while abs(old - Result) <= 1e-9:
            Result = M + e * math.sin(old)
            old = Result
        return Result

    def get_arg_lat(self):
        pass


def siderial_time0(val):
    d = julian.to_jd(val)
    t = (math.modf(d)[1] - JD2000)/36525
    r = (3.879333e-4 * t + 360000.7700536)*t+101.25228375
    n = math.modf(r/360)[1]
    r = r-n*360
    return math.radians(r)

def siderial_time(val):
    dt = (val - datetime.combine(val.date(),time(0))).total_seconds()
    so = siderial_time0(val)
    r = so + We * (1-0.002737903)*dt
    return r

def mabs_geo(val):
    matrix = np.zeros((3, 3))
    matrix[0][0] = math.cos(val)
    matrix[0][1] = math.sin(val)
    matrix[1][0] = -matrix[0][1]
    matrix[1][1] = matrix[0][0]
    matrix[2][2] = 1.0
    return matrix


spacecraft = Spacecraft(datetime(year=2018, month=10, day=18, hour=15))
spacecraft.inital_orbit.assign(7000.0, 0.01, 0.0, math.radians(57.3), 0, math.radians(270))

for i in range(0, 15000, 60):
    print(spacecraft.current_orbit.Pos[0], spacecraft.current_orbit.Pos[1], spacecraft.current_orbit.Pos[2])
    spacecraft.set_current_time(i)

